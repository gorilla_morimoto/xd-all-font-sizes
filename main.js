const { Text, Artboard, Color } = require('scenegraph');

function main(_, root) {
  root.children.forEach(node => {
    if (node instanceof Artboard) {
      const filteredNode = node.children.filter(n => n instanceof Text);
      const res = filteredNode.map(n => n.fontSize);
      const s = new Set(res);
      const a = Array.from(s).sort((a, b) => a - b);

      const t = new Text();
      t.text = `[${a.join(',')}]`;
      t.fill = new Color('#FF0000');
      t.fontSize = 24;
      node.addChild(t);
      t.moveInParentCoordinates(30 , 50);
    }
  });
}

module.exports = {
  commands: {
    fontSize: main
  }
};
